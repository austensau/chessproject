//
//  LandingScene.swift
//  chess game
//
//  Created by Student User on 4/1/16.
//  Copyright © 2016 student. All rights reserved.
//

import SpriteKit

class LandingScene: SKScene {
    var parentView: GameViewController?
    //button secondary color: #0f416e
    
    override func didMoveToView(view: SKView) {
        self.backgroundColor = UIColor.darkGrayColor()
        drawScene()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            guard let touchedNode = self.nodeAtPoint(location) as? SKSpriteNode
                else {
                    return
            }
            
            if touchedNode.name == "New Game" {
                parentView!.newButtonPressed()
            } else if touchedNode.name == "Continue" {
                parentView!.continueButtonPressed()
            } else if touchedNode.name == "Multiplayer" {
                //do stuff
            } else if touchedNode.name == "Quit" {
                //tell parentView to quit app
            }
        }
    }
    
    func drawScene() {
        //x-center, y-1/3 from bottom
        let xOffset:CGFloat = size.width / 2
        let yOffset:CGFloat = size.height / 3
        let buttonHeight: CGFloat
        
        var button = SKSpriteNode(imageNamed: "newgame")
        let ratio = button.size.width / button.size.height
        button.size.width = size.width / 4
        button.size.height = button.size.width / ratio
        buttonHeight = button.size.height
        button.position = CGPointMake(xOffset, yOffset)
        button.name = "New Game"
        self.addChild(button)
        
        button = SKSpriteNode(imageNamed: "continue")
        setButtonSize(button, height: buttonHeight)
        button.position = CGPointMake(xOffset, yOffset - button.size.height - 10)
        button.name = "Continue"
        self.addChild(button)

        button = SKSpriteNode(imageNamed: "multiplayer")
        setButtonSize(button, height: buttonHeight)
        button.position = CGPointMake(xOffset, yOffset - (2 * button.size.height) - 20)
        button.name = "Mutliplayer"
        self.addChild(button)
        
        button = SKSpriteNode(imageNamed: "quit")
        setButtonSize(button, height: buttonHeight)
        button.position = CGPointMake(xOffset, size.height / 8)
        button.name = "Quit"
        self.addChild(button)
        
        button = SKSpriteNode(imageNamed: "title")
        setButtonSize(button, height: buttonHeight)
        button.position = CGPointMake(xOffset, size.height * 0.8)
        button.name = "Title"
        self.addChild(button)
        
        
        
    }
    
    func setButtonSize(button: SKSpriteNode, height: CGFloat) {
        let ratio = button.size.width / button.size.height
        button.size.height = height
        button.size.width = height * ratio
    }
}