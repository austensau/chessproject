//
//  PromotionScene.swift
//  chess game
//
//  Created by Student User on 4/10/16.
//  Copyright © 2016 student. All rights reserved.
//

import SpriteKit

class PromotionScene: SKScene {
    private let pieceName = ["rook", "bishop", "knight", "queen"]

    var parentView: GameViewController?
    var parentScene: GameScene?
    var team = ""
    
    override func didMoveToView(view: SKView) {
        self.backgroundColor = UIColor.darkGrayColor()
        drawScene()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            guard var touchedNode = self.nodeAtPoint(location) as? SKSpriteNode
                else {
                    return
            }
            
            if touchedNode.parent != self {
                touchedNode = touchedNode.parent as! SKSpriteNode
            }
            
            parentScene?.promotedPieceName = (touchedNode.children.first?.name)!
            parentView?.continueButtonPressed()
        }
    }
    
    func drawScene() {
        let squareSize = CGSize(width: size.width / 4, height: size.width / 4)
        let xOffset:CGFloat = squareSize.width / 2
        let yOffset:CGFloat = size.height / 2
        
        // Used to alternate between white and black squares
        var toggle:Bool = false
        
        for file in 1...4 {
            let color = toggle ? SKColor.whiteColor() : SKColor.blackColor()
            let square = SKSpriteNode(color: color, size: squareSize)
            square.position = CGPointMake(CGFloat(file - 1) * squareSize.width + xOffset,
                yOffset)
            square.name = "\(file)"
            self.addChild(square)
            toggle = !toggle
        }

        for piece in 0...3 {
            let gamePiece = SKSpriteNode(imageNamed: "\(team)\(pieceName[piece])")
            let square = self.childNodeWithName("\(piece + 1)") as! SKSpriteNode?
            
            let ratio = gamePiece.size.height / gamePiece.size.width
            gamePiece.size.height = 0.85 * square!.size.height
            gamePiece.size.width = gamePiece.size.height / ratio
            gamePiece.name = "\(team)\(pieceName[piece])"
            square?.addChild(gamePiece)
        }
        
        let label = SKLabelNode(text: "Select a piece to promote your pawn.")
        label.position = CGPoint(x: size.width / 2, y: size.height * 0.80)
        //fit label to fram
        while size.width < label.frame.width {
            label.fontSize -= 1.0
        }
        addChild(label)
        
    }
    
}
