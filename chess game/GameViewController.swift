//
//  GameViewController.swift
//  chess game
//
//  Created by student on 3/23/16.
//  Copyright (c) 2016 student. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    var boardScene: GameScene?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let scene = LandingScene(fileNamed: "GameScene") {
            scene.parentView = self
            showScene(scene)
        }
    }

    override func shouldAutorotate() -> Bool {
        return false
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func continueButtonPressed() {
        showScene(boardScene!)
    }
    
    func newButtonPressed() {
        boardScene = GameScene(fileNamed: "GameScene")
        boardScene!.parentView = self
        showScene(boardScene!)
    }
    
    func showScene(scene: SKScene) {
        // Configure the view.
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        /* Set the scale mode to scale to fit the window */
        scene.scaleMode = .ResizeFill
        
        skView.presentScene(scene)
    }
}
