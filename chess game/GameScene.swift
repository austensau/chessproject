//
//  GameScene.swift
//  chess game
//
//  Created by student on 3/23/16.
//  Copyright (c) 2016 student. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    var parentView: GameViewController?
    var promotedPieceName: String?
    var piecePromoted: String?
    var boardDrawn = false
    let board = Board()
	let files = ["a", "b", "c", "d", "e", "f", "g", "h"]
	var playerTurn = "b"
	//.0 = bool Selected, .1 = name, .2 = origonal color
	var pieceSelected = (false, "", SKColor.blackColor())
	
    override func didMoveToView(view: SKView) {
        if !boardDrawn {
            boardDrawn = true
            self.backgroundColor = UIColor.darkGrayColor()
            drawBoard()
            addPieces()
            board.boardScene = self
        }
        if promotedPieceName != nil {
            swapPromotedPiece()
            promotedPieceName = nil
            piecePromoted = nil
        }
    }
    
    //TODO break apart
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
			guard var touchedNode = self.nodeAtPoint(location) as? SKSpriteNode
				else {
					return
			}
            
            handleButtonPresses(touchedNode)
			
			//self = gamescene, if parent != gamescene (i.e. parent == square) then
			//set touchedNode to its parent square
			if touchedNode.parent != self {
				touchedNode = touchedNode.parent as! SKSpriteNode
			}
			
			//if no piece selected & touchedNode has a child
			if !pieceSelected.0 && !touchedNode.children.isEmpty {
				let pieceName = touchedNode.children.first?.name
				let index = pieceName!.startIndex.advancedBy(0)
				//if piece is same team of whose turn it is
				if pieceName![index] == playerTurn[index] {
					//highlight new selected square
					pieceSelected.0 = true
					pieceSelected.1 = touchedNode.name!
					pieceSelected.2 = touchedNode.color
					touchedNode.color = SKColor.redColor()
				}
			
			} else if pieceSelected.0 { //if piece is selected
				//if space is occupied
				if !touchedNode.children.isEmpty {
					let pieceName = touchedNode.children.first?.name
					let index = pieceName!.startIndex.advancedBy(0)
					//if piece is same team of whose turn it is to move
					if pieceName![index] == playerTurn[index] {
						//unhilight old piece
						let oldPiece = getSquare(pieceSelected.1)
						oldPiece?.color = pieceSelected.2
						//highlight new piece
						pieceSelected.1 = touchedNode.name!
						pieceSelected.2 = touchedNode.color
						touchedNode.color = SKColor.redColor()
					} else { //attempt to attack enemy piece
                        let pieceName = getSquare(pieceSelected.1)?.children.first?.name
                        if board.moveIsValid(pieceName!, origin: pieceSelected.1, destination: touchedNode.name!) {
                            //move sprite to destination
                            let destinationChild = getSquare(touchedNode.name!)?.children.first
                            destinationChild?.removeFromParent()
                            let child = getSquare(pieceSelected.1)!.children.first
                            child?.removeFromParent()
                            touchedNode.addChild(child!)
                            
                            if playerTurn == "b" {
                                playerTurn = "w"
                            } else {
                                playerTurn = "b"
                            }
                            getSquare(pieceSelected.1)!.color = pieceSelected.2
                            pieceSelected.0 = false
                        }
					}
				} else { //space unoccupied, attempt to move
                    let pieceName = getSquare(pieceSelected.1)?.children.first?.name
                    if board.moveIsValid(pieceName!, origin: pieceSelected.1, destination: touchedNode.name!) {
                        //move sprite to destination
                        let child = getSquare(pieceSelected.1)!.children.first!
                        child.removeFromParent()
                        touchedNode.addChild(child)
                        if playerTurn == "b" {
                            playerTurn = "w"
                        } else {
                            playerTurn = "b"
                        }
                        getSquare(pieceSelected.1)!.color = pieceSelected.2
                        pieceSelected.0 = false
                    }
				}
			}
        }
		
    }
    
    func handleButtonPresses(touchedNode: SKSpriteNode) {
        if touchedNode.name == "Save & Quit" {
            //save stuff
            let transition = SKTransition.revealWithDirection(.Down, duration: 1.0)
            
            let nextScene = LandingScene(size: scene!.size)
            nextScene.scaleMode = .ResizeFill
            nextScene.parentView = parentView
            
            scene?.view?.presentScene(nextScene, transition: transition)
        } else if touchedNode.name == "Forfeit" {
            if playerTurn == "b" {
                print("white wins")
            } else {
                print("black wins")
            }
            let transition = SKTransition.revealWithDirection(.Down, duration: 1.0)
            
            let nextScene = LandingScene(size: scene!.size)
            nextScene.scaleMode = .ResizeFill
            
            scene?.view?.presentScene(nextScene, transition: transition)
        } else if touchedNode.name == "History" {
            //let transition = SKTransition.revealWithDirection(.Down, duration: 1.0)
            
            //let nextScene = HistoryScene(size: scene!.size)
            //nextScene.scaleMode = .AspectFill
            
            //scene?.view?.presentScene(nextScene, transition: transition)
        }
    }
    
    func setPromotedPiece(pieceName: String) {
        let transition = SKTransition.revealWithDirection(.Down, duration: 1.0)
        let nextScene = PromotionScene(fileNamed: "GameScene")
        nextScene?.scaleMode = .ResizeFill
        nextScene!.team = playerTurn
        nextScene?.parentView = parentView
        nextScene?.parentScene = self
        
        piecePromoted = pieceName
        
        scene?.view?.presentScene(nextScene!, transition: transition)
    }
    
    func swapPromotedPiece() {
        var square: SKSpriteNode?
        var count = 0
        while square == nil {
            //find the square with a pawn on the back row
            if self.childNodeWithName("\(files[count])1")?.children.first?.name == piecePromoted! {
                square = self.childNodeWithName("\(files[count])1") as? SKSpriteNode
            } else if self.childNodeWithName("\(files[count])8")?.children.first?.name == piecePromoted! {
                square = self.childNodeWithName("\(files[count])8") as? SKSpriteNode
            }
            count++

        }
        square?.removeAllChildren()
        let newPiece = SKSpriteNode(imageNamed: promotedPieceName!)
        newPiece.name = promotedPieceName
        
        let ratio = newPiece.frame.width / newPiece.frame.height
        newPiece.size.height = square!.frame.height * 0.85 //height = 85% of square
        newPiece.size.width = newPiece.frame.height * ratio
        
        square?.addChild(newPiece)
        board.pawnPromoted(promotedPieceName!, origin: (square?.name)!)
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func drawBoard() {
        // Board parameters
        let numRows = 8
        let numCols = 8
        let squareSize = CGSizeMake(size.width / 8, size.width / 8)
        var xOffset:CGFloat = squareSize.width / 2
        //math: middle of screen = line between row 4 and row 5
        var yOffset:CGFloat = (squareSize.height / 2) + ((size.height / 2) - (4 * squareSize.height))
		
        // Used to alternate between white and black squares
        var toggle:Bool = false
        
        for rank in 1...numRows {
            for file in 1...numCols {
                // Letter for this file
                let letter = files[file - 1]
                // Determine the color of square
                let color = toggle ? SKColor.whiteColor() : SKColor.blackColor()
                let square = SKSpriteNode(color: color, size: squareSize)
                square.position = CGPointMake(CGFloat(file - 1) * squareSize.width + xOffset,
                    CGFloat(rank - 1) * squareSize.height + yOffset)
                // Set squares name based on file(a-h), rank(1-8)
                square.name = "\(letter)\(rank)"
                self.addChild(square)
                toggle = !toggle
            }
            toggle = !toggle
        }
        
        xOffset = 0
        yOffset = size.height
        let buttonHeight: CGFloat
        
        var button = SKSpriteNode(imageNamed: "save&quit")
        var ratio = button.size.width / button.size.height
        button.size.width = size.width / 4
        button.size.height = button.size.width / ratio
        buttonHeight = button.size.height
        xOffset = button.size.width / 2
        button.position = CGPointMake(xOffset, yOffset - button.size.height / 2)
        button.name = "Save & Quit"
        self.addChild(button)
        
        button = SKSpriteNode(imageNamed: "forfeit")
        ratio = button.size.width / button.size.height
        button.size.height = buttonHeight
        button.size.width = buttonHeight * ratio
        button.position = CGPointMake(2 * xOffset + button.size.width / 2, yOffset - button.size.height / 2)
        button.name = "Forfeit"
        self.addChild(button)
        
        button = SKSpriteNode(imageNamed: "history")
        ratio = button.size.width / button.size.height
        button.size.height = buttonHeight
        button.size.width = buttonHeight * ratio
        button.position = CGPointMake(size.width - button.size.width / 2, yOffset  - button.size.height / 2)
        button.name = "History"
        self.addChild(button)
    }
	
	func getSquare(name: String) -> SKSpriteNode? {
		let square: SKSpriteNode? = self.childNodeWithName(name) as! SKSpriteNode?
		return square
	}
	
	func addPieces() {
		var color = "b"
		//add pawns
		for rank in 0...1 {
            //other side of board, team is white
			if rank == 1 {
				color = "w"
			}
            
            for file in 1...8 {
                //add pawn
                if let square = getSquare("\(files[file - 1])\((rank * 5) + 2)") {
                    let gamePiece = SKSpriteNode(imageNamed: "\(color)pawn")
                    gamePiece.name = "\(color)pawn"
                    let ratio = gamePiece.frame.width / gamePiece.frame.height
                    gamePiece.size.height = square.frame.height * 0.85 //height = 85% of square
                    gamePiece.size.width = gamePiece.frame.height * ratio
                    square.addChild(gamePiece)
                }
                
                //add other
                if let square = getSquare("\(files[file - 1])\((rank * 7) + 1)") {
                    var gamePiece: SKSpriteNode
                    if file == 1 || file == 8 {
                        gamePiece = SKSpriteNode(imageNamed: "\(color)rook")
                        gamePiece.name = "\(color)rook"
                    } else if file == 2 || file == 7 {
                        gamePiece = SKSpriteNode(imageNamed: "\(color)knight")
                        gamePiece.name = "\(color)knight"
                    } else if file == 3 || file == 6 {
                        gamePiece = SKSpriteNode(imageNamed: "\(color)bishop")
                        gamePiece.name = "\(color)bishop"
                    } else if file == 4 {
                        gamePiece = SKSpriteNode(imageNamed: "\(color)king")
                        gamePiece.name = "\(color)king"
                    } else { //file == 5
                        gamePiece = SKSpriteNode(imageNamed: "\(color)queen")
                        gamePiece.name = "\(color)queen"
                    }
                    let ratio = gamePiece.frame.width / gamePiece.frame.height
                    gamePiece.size.height = square.frame.height * 0.85 //height = 85% of square
                    gamePiece.size.width = gamePiece.frame.height * ratio
                    square.addChild(gamePiece)
                }
            }
		}
	}
}
