//
//  Board.swift
//  chess game
//
//  Created by student on 4/1/16.
//  Copyright © 2016 student. All rights reserved.
//

import SpriteKit

//TODO castling, en passant capture, promotion, checkmate, stalemate
class Board {
    var boardScene: GameScene?
    //dictionary for converting files to an index in board
    private let fileToInt = ["a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7]
    var blackInCheck = false
    var whiteInCheck = false
    
    //2-d array "piecename" @ board[file][rank]
    private var board = Array<Array<String>>()
    
    init() {
        for _ in 1...8 { //initialize an 8x8 array
            board.append(Array(count: 8, repeatedValue: ""))
        }
        
        var team = "b"
        for rank in 0...1 {
            //change teams on other side of board
            if rank == 1 {
                team = "w"
            }

            //initialize piece positions
            for file in 0...7 {
                //rank 1 & 6 are pawn rows for 0 based indexing
                board[file][(rank * 5) + 1] = "\(team)pawn"
                
                //ranks 0 & 7 are rows for rest of pieces
                if file == 0 || file == 7 {
                    board[file][rank * 7] = "\(team)rook"
                } else if file == 1 || file == 6 {
                    board[file][rank * 7] = "\(team)knight"
                } else if file == 2 || file == 5 {
                    board[file][rank * 7] = "\(team)bishop"
                } else if file == 3 {
                    board[file][rank * 7] = "\(team)king"
                } else if file == 4 {
                    board[file][rank * 7] = "\(team)queen"
                }
            }
            
        }
    }
    
    func moveIsValid(pieceType: String, origin: String, destination: String) -> Bool {
        var coordinates = getCoordinates(origin, destination: destination)
        var swap = false
        var promote = false
        
        let indexOfTeam = pieceType.startIndex.advancedBy(1)
        var team = pieceType.substringToIndex(indexOfTeam)
        
        //test if a move would put the mover in check and abort if it would
        let destinationPiece = board[coordinates[2]][coordinates[3]]
        swapPieces(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
        if(findCheck(team)) {
            //unswap and return
            swapPieces(coordinates[2], originRank: coordinates[3], destinationFile: coordinates[0], destinationRank: coordinates[1])
            board[coordinates[2]][coordinates[3]] = destinationPiece
            return false
        } else { //unswap pieces
            swapPieces(coordinates[2], originRank: coordinates[3], destinationFile: coordinates[0], destinationRank: coordinates[1])
            board[coordinates[2]][coordinates[3]] = destinationPiece
        }
        
        if pieceType.containsString("pawn") {
            swap = movePawn(team, originFile: coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
            swap = true
            if swap {
                if team == "b" && coordinates[3] == 7 {
                    promote = true
                } else if team == "w" && coordinates[3] == 0 {
                    promote = true
                }
            }
        } else if pieceType.containsString("rook") {
            swap = moveRook(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
        } else if pieceType.containsString("knight") {
            swap = moveKnight(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
        } else if pieceType.containsString("bishop") {
            swap = moveBishop(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
        } else if pieceType.containsString("king") {
            swap = moveKing(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
        } else if pieceType.containsString("queen"){
            swap = moveQueen(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
        }
        
        if swap {
            swapPieces(coordinates[0], originRank: coordinates[1], destinationFile: coordinates[2], destinationRank: coordinates[3])
            
            //check if the other team has been placed in check
            if(team == "b") {
                team = "w"
                whiteInCheck = findCheck(team)
            } else {
                team = "b"
                blackInCheck = findCheck(team)
            }
            
            if promote {
                promotePawn(coordinates[2], originRank: coordinates[3])
            }
            
            return true

        }
        
        return false
    }
    
    func playerInCheck() -> Bool {
        if blackInCheck || whiteInCheck {
            return true
        }
        return false
    }
    
    //pawn moves retardedly
    private func movePawn(team: String, originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) -> Bool {
        var direction = 1
        
        //black pawns move in positive direction, white in negative
        if team == "w" {
            direction = -1
        }
        
        //if destination occupied
        if board[destinationFile][destinationRank] != "" {
            //if destination attackable
            if originRank + direction == destinationRank {
                if originFile == destinationFile - 1 || originFile == destinationFile + 1 {
                    return true
                }
            }
        } else {
            if originFile == destinationFile {
                //if moving 1 forward
                if originRank + direction == destinationRank{
                    return true
                //if moving 2 forward
                } else if originRank + (2 * direction) == destinationRank {
                    //if point inbetween not occupied
                    if board[originFile][originRank + direction] == "" {
                        //and if piece is on 'homerow'
                        if team == "b" && originRank == 1 {
                            return true
                        } else if team == "w" && originRank == 6 {
                            return true
                        }
                    }
                }
            }
        }
        
        return false
    }
    
    //rook moves when aligned with destination and path inbetween is clear
    private func moveRook(originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) -> Bool {
        //direction positive means rank or file increasing
        var direction = 1
        
        if originFile == destinationFile {
            if originRank > destinationRank {
                direction = -1 //move from higher rank to lower rank
            }
            
            //max distance between two points is 7 min is 1
            for rank in 1...7 {
                //if not at destination
                if originRank + (rank * direction) != destinationRank {
                    //non-empty string indicates path is blocked by another piece
                    if board[originFile][originRank + (rank * direction)] != "" {
                        return false
                    }
                } else { //indicates path is clear
                    return true
                }
            }
        } else if originRank == destinationRank { //mirror of above code
            if originFile > destinationFile {
                direction = -1
            }
            
            for file in 1...7 {
                if originFile + (file * direction) != destinationFile {
                    if board[originFile + (file * direction)][originRank] != "" {
                        return false
                    }
                } else {
                    return true
                }
            }
        }
        
        return false //no straight path to destination
    }
    
    //knight moves whend file/rank offset by magnitude 1/2 or 2/1
    private func moveKnight(originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) -> Bool {
        if originFile == destinationFile + 1 || originFile == destinationFile - 1 {
            if originRank == destinationRank + 2 || originRank == destinationRank - 2 {
                return true
            }
        } else if originRank == destinationRank + 1 || originRank == destinationRank - 1 {
            if originFile == destinationFile + 2 || originFile == destinationFile - 2 {
                return true
            }
        }
        
        return false
    }
    
    //bishop moves when diagonally aligned with destination and path inbetween is clear
    private func moveBishop(originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) -> Bool {
        var fileDirection = 1
        var rankDirection = 1
        var fileDistance = destinationFile - originFile
        var rankDistance = destinationRank - originRank
        
        if fileDistance < 0 {
            fileDirection = -1
            fileDistance *= -1
        }
        if rankDistance < 0 {
            rankDirection = -1
            rankDistance *= -1
        }
        
        //if origin diagonal to destination
        if fileDistance == rankDistance {
            for i in 1...7 {
                //when the file matches destination it's assumed the rank matches too
                if originFile + (i * fileDirection) != destinationFile {
                    //check if position diagonal to origin is occupied
                    if board[originFile + (i * fileDirection)][originRank + (i * rankDirection)] != "" {
                        return false //path blocked
                    }
                } else { //path to destination clear
                    return true
                }
            }
        }
        
        return false //destination not diagonal to origin
    }
    
    //king moves 1 point in any direction
    private func moveKing(originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) -> Bool {
        let fileDistance = destinationFile - originFile
        let rankDistance = destinationRank - originRank
        
        //can move anywhere when rank & file distances between -1 and 1
        if fileDistance < 2 && fileDistance > -2 {
            if rankDistance < 2 && rankDistance > -2 {
                return true
            }
        }
        
        return false
    }
    
    //queen moves when it's a legal bishop or rook move
    private func moveQueen(originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) -> Bool {
        if moveBishop(originFile, originRank: originRank, destinationFile: destinationFile, destinationRank: destinationRank) || moveRook(originFile, originRank: originRank, destinationFile: destinationFile, destinationRank: destinationRank) {
            return true
        }
        
        return false //if not legal bishop or rook move, it's not a legal  queen move
    }
    
    private func findCheck(team: String) -> Bool {
        var enemy = ""
        var kingFile = 0
        var kingRank = 0
        
        //determine enemy team
        if team == "b" {
            enemy = "w"
        } else {
            enemy = "b"
        }
        
        //search board for position of teams king
        for i in 0...7 {
            for j in 0...7 {
                if board[i][j] == "\(team)king" {
                    kingFile = i
                    kingRank = j
                    break
                }
            }
        }
        
        //cast a ray in all 8 directions from the king, determine if piece can attack king
        //directions = (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1), (1, -1)
        for i in -1...1 {
            for j in -1...1 {
                //skip (0, 0)
                if i == 0 && j == 0 {
                    continue
                }
                //max possible distance to a piece = 7
                for distance in 1...7 {
                    //bounds checking
                    if kingFile + (distance * i) < 0 || kingRank + (distance * j) < 0 || kingFile + (distance * i) > 7 || kingRank + (distance * j) > 7{
                        break
                    }
                    let piece = board[kingFile + (distance * i)][kingRank + (distance * j)]
                    //when a piece is encountered
                    if piece != "" {
                        if i == j || i == -j { //if it's diagonal to the king
                            if piece == "\(enemy)bishop" || piece == "\(enemy)queen" {
                                return true
                            } else if piece == "\(enemy)pawn"{
                                //if the enemy pawn can attack the king check has been found
                                if movePawn(enemy, originFile: kingFile + (distance * i), originRank: kingRank + (distance * j), destinationFile: kingFile, destinationRank: kingRank) {
                                    return true
                                }
                            } else if piece == "\(enemy)king" {
                                if moveKing(kingFile + (distance * i), originRank: kingRank + (distance * j), destinationFile: kingFile, destinationRank: kingRank) {
                                    return true
                                }
                            }
                        } else { //else it's adjacent to the king
                            if piece == "\(enemy)rook" || piece == "\(enemy)queen" {
                                return true
                            } else if piece == "\(enemy)king" { //
                                if moveKing(kingFile + (distance * i), originRank: kingRank + (distance * j), destinationFile: kingFile, destinationRank: kingRank) {
                                    return true
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
        
        //lastly check possible positions of enemy knights
        //relativePositions = (2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2), (1, -2), (2, -1)
        for i in -2...2 {
            if i == 0 { //skip(j, 0)
                continue
            }
            for j in -2...2 {
                if j == 0 { //skip (i, 0)
                    continue
                }
                if i == j || i == -j { //skip (1, 1), (1, -1), (-1, -1), (-1, 1), (-2, -2), (-2, 2), (2, -2), (2, 2)
                    continue
                }
                //bounds checking
                if kingFile + i < 0 || kingRank + j < 0 || kingFile + i > 7 || kingRank + j > 7 {
                    continue
                }
                if board[kingFile + i][kingRank + j] == "\(enemy)knight" {
                    return true
                }
            }
        }
        
        //check not found
        return false
    }
    
    func promotePawn(originFile: Int, originRank: Int) {
        //upgrade pawn at origin to value returned from boardScene
        print("promoting \(board[originFile][originRank])")
        boardScene!.setPromotedPiece(board[originFile][originRank])
    }
    
    func pawnPromoted(promotedName: String, origin: String) {
        let coordinates = getCoordinates(origin, destination: "a0")
        let originFile = coordinates[0]
        let originRank = coordinates[1]
        board[originFile][originRank] = promotedName
    }
    
    //move a piece on the board, leaving the origin empty
    private func swapPieces(originFile: Int, originRank: Int, destinationFile: Int, destinationRank: Int) {
        board[destinationFile][destinationRank] = board[originFile][originRank]
        board[originFile][originRank] = ""
    }
    
    //converts two strings representing origin/destination into integer values for use as indexes
    private func getCoordinates(origin: String, destination: String) -> [Int] {
        let indexOfFile = origin.startIndex.advancedBy(0)
        let originFile = fileToInt["\(origin[indexOfFile])"]!
        let destinationFile = fileToInt["\(destination[indexOfFile])"]!
        let originRank = Int(origin.substringFromIndex(indexOfFile.advancedBy(1)))! - 1
        let destinationRank = Int(destination.substringFromIndex(indexOfFile.advancedBy(1)))! - 1
        
        return [originFile, originRank, destinationFile, destinationRank]
    }
}